%define name loggerdemo
%define version 1.0.0
%define release 01

Summary: MSL Server logger demo
Name: %{name}
Version: %{version}
Release: %{release}
License: GPL
Group: Networking/Daemons
Source: %{name}-%{version}.tar.gz
Packager: Neftali Yagua <neftali.yagua@tollring.com>
BuildRoot: /var/tmp/%{name}-%{version}-%{release}-buildroot
BuildArchitectures: noarch

%description
Logger Demo sample application.

%changelog
* Thu Feb 2 2006 Neftali Yagua <neftali.yagua@tollring.com>
- 1.0.0-01
- Original version
%prep
%setup -q

%build
perl createlinks

DEFAULTS=root/etc/e-smith/db/configuration/defaults/loggerdemo
mkdir -p $DEFAULTS

echo "service"> $DEFAULTS/type
echo "enabled"> $DEFAULTS/status
echo "10"> $DEFAULTS/Interval

%install
rm -rf $RPM_BUILD_ROOT
(cd root ; find . -depth -print | cpio -dump $RPM_BUILD_ROOT)
rm -f %{name}-%{version}-filelist
/sbin/e-smith/genfilelist $RPM_BUILD_ROOT > %{name}-%{version}-filelist

%clean
rm -rf $RPM_BUILD_ROOT

%post
/etc/e-smith/events/actions/initialize-default-databases
/etc/e-smith/events/actions/navigation-conf
/sbin/e-smith/expand-template /etc/crontab
true

%postun
/sbin/e-smith/expand-template /etc/crontab
true

%files -f %{name}-%{version}-filelist
%defattr(-,root,root)