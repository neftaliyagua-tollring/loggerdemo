#!/usr/bin/perl -w
#----------------------------------------------------------------------
# heading : Demo
# description : Logger
# navigation : 1000 1000
#----------------------------------------------------------------------
package esmith::FormMagick::Panel::loggerdemoFM;
use strict;

use warnings;
use esmith::ConfigDB;
use esmith::FormMagick;
our @ISA = qw(esmith::FormMagick Exporter);
our @EXPORT = qw();
our $VERSION = sprintf '%d.%03d', q$Revision: 1.1 $ =~ /: (\d+).(\d+)/;
our $db = esmith::ConfigDB->open or die "Couldn't open ConfigDB\n";
sub get_status
{
    return $db->get_prop("loggerdemo", "status");
}
sub get_interval
{
    return $db->get_prop("loggerdemo", "Interval");
}
sub change_settings
{
    my $fm = shift;
    my $q = $fm->{'cgi'};

    $db->set_prop('loggerdemo', 'status', $q->param("loggerdemo_status"));
    $db->set_prop('loggerdemo', 'Interval', $q->param("loggerdemo_Interval"));
    unless ( system ("/sbin/e-smith/expand-template", "/etc/crontab") == 0 )
    {
        $fm->error('ERROR_UPDATING');
        return undef;
    }
    $fm->success('SUCCESS');
}
1;