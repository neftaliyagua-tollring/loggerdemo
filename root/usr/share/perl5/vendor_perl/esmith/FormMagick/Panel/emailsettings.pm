sub change_settings_delivery
{
    my ($fm) = @_;
    my $q = $fm->{'cgi'};
    my $admin_email = $q->param('AdminEmail');
    my $admin = $accounts->get('admin');
    if ($admin_email)
    {
        $admin->merge_props(
            EmailForward => 'forward',
            ForwardAddress => $admin_email,
        );
    }
    else
    {
        $admin->merge_props(
            EmailForward => 'local',
            ForwardAddress => '',
        );
    }
    unless ( system( "/sbin/e-smith/signal-event", "email-update" ) == 0 )
    {
        $fm->error('ERROR_UPDATING');
        return undef;
    }
    $fm->success('SUCCESS');
}